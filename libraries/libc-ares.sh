#!/usr/bin/env bash

set -ex

cares_ver=1.34.4

cd /root
wget https://github.com/c-ares/c-ares/releases/download/v$cares_ver/c-ares-$cares_ver.tar.gz

tar -xvf c-ares-$cares_ver.tar.gz
cd c-ares-$cares_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --enable-shared=no
make -j$(($(nproc)+1))
make install
