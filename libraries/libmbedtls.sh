#!/usr/bin/env bash

set -ex

mbedtls_ver=2.28.9

cd /root
wget https://github.com/Mbed-TLS/mbedtls/archive/refs/tags/v$mbedtls_ver.tar.gz

tar -xvf v$mbedtls_ver.tar.gz
cd mbedtls-$mbedtls_ver

make -j$(($(nproc)+1)) install CFLAGS="-fPIC" DESTDIR=/usr
