#!/usr/bin/env bash

set -ex

cd /root
wget https://cfhcable.dl.sourceforge.net/project/pcre/pcre/8.45/pcre-8.45.tar.gz

tar -xvf pcre-8.45.tar.gz
cd pcre-8.45

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --enable-shared=no
make -j$(($(nproc)+1))
make install
