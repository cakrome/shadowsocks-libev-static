#!/usr/bin/env bash

set -ex

libsodium_ver=1.0.20

cd /root
wget https://github.com/jedisct1/libsodium/releases/download/$libsodium_ver-RELEASE/libsodium-$libsodium_ver.tar.gz

tar -xvf libsodium-$libsodium_ver.tar.gz
cd libsodium-$libsodium_ver

CFLAGS="-fPIC -O3" CPPFLAGS="-fPIC -O3" ./configure --prefix=/usr --enable-shared=no
make -j$(($(nproc)+1))
make install
