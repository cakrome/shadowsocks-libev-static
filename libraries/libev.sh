#!/usr/bin/env bash

set -ex

cd /root
wget http://dist.schmorp.de/libev/libev-4.33.tar.gz

tar -xvf libev-4.33.tar.gz
cd libev-4.33

./configure --disable-shared
make -j$(($(nproc)+1))
make install
