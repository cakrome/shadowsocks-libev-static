FROM docker.io/centos:7
LABEL maintainer="CaKrome <cakrome@protonmail.com>"

ARG SHADOWSOCKS_VERSION
ENV SOURCE_URL=https://github.com/shadowsocks/shadowsocks-libev/releases/download/v${SHADOWSOCKS_VERSION}/shadowsocks-libev-${SHADOWSOCKS_VERSION}.tar.gz

WORKDIR /root

COPY fix_repo.sh /root/fix_repo.sh

RUN /root/fix_repo.sh

RUN set -ex \
    && yum update -y \
    && yum -y install yum-utils \
    && yum-config-manager --enable extras \
    && yum install -y centos-release-scl \
    && /root/fix_repo.sh \
    && yum update -y \
    && yum install -y wget file python3 make gcc gcc-c++ gdb devtoolset-10 devtoolset-10-make devtoolset-10-gcc devtoolset-10-gcc-c++  \
    devtoolset-10-gdb pkgconfig asciidoc xmlto

COPY libraries/libsodium.sh /root/libsodium.sh
COPY libraries/libmbedtls.sh /root/libmbedtls.sh
COPY libraries/libc-ares.sh /root/libc-ares.sh
COPY libraries/libev.sh /root/libev.sh
COPY libraries/libpcre.sh /root/libpcre.sh

RUN set -ex \
    && source scl_source enable devtoolset-10 \
    && /root/libsodium.sh \
    && /root/libmbedtls.sh \
    && /root/libc-ares.sh \
    && /root/libev.sh \
    && /root/libpcre.sh \
    && cd /root \
    && wget ${SOURCE_URL} \
    && tar -xvf shadowsocks-libev-${SHADOWSOCKS_VERSION}.tar.gz \
    && cd shadowsocks-libev-${SHADOWSOCKS_VERSION} \
    && ./configure --prefix=/root/build/shadowsocks-libev-${SHADOWSOCKS_VERSION} \
    && make -j$(($(nproc)+1)) \
    && make install

RUN set -ex \
    && cd /root/build \
    && tar -cf shadowsocks-libev-${SHADOWSOCKS_VERSION}.tar shadowsocks-libev-${SHADOWSOCKS_VERSION}/ \
    && xz --threads=$(($(nproc)+1)) shadowsocks-libev-${SHADOWSOCKS_VERSION}.tar
