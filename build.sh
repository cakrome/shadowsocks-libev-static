#!/usr/bin/env bash

set -euo pipefail

ss_ver=3.3.5

sudo docker build "$@" -f Dockerfile . -t cakrome/shadowsocks-libev-static --build-arg SHADOWSOCKS_VERSION=$ss_ver
id=$(sudo docker create cakrome/shadowsocks-libev-static)
sudo docker cp "$id":/root/build/shadowsocks-libev-$ss_ver.tar.xz .
sudo docker rm -v "$id"
sudo chown "$USER":"$USER" shadowsocks-libev-$ss_ver.tar.xz
